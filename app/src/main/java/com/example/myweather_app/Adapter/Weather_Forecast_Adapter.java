package com.example.myweather_app.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myweather_app.Common.Common;
import com.example.myweather_app.Model.WeatherForecastResult;
import com.example.myweather_app.R;
import com.squareup.picasso.Picasso;

public class Weather_Forecast_Adapter extends RecyclerView.Adapter<Weather_Forecast_Adapter.MyViewHolder> {

    Context context;
    WeatherForecastResult  weatherForecastResult ;

    public Weather_Forecast_Adapter(Context context, WeatherForecastResult weatherForecastResult) {
        this.context = context;
        this.weatherForecastResult = weatherForecastResult;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from( context ).inflate( R.layout.list_weather_forecast  ,viewGroup ,false);
        return new MyViewHolder( view );
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {


        //Load Image
        Picasso.get().load( new StringBuffer("https://openweathermap.org/img/w/")
               .append(weatherForecastResult.getList().get( i ).weather.get(0).getIcon())
                .append(".png").toString()).into( myViewHolder.imageViewForecast );

        //Load Data

        myViewHolder.day.setText( new StringBuffer(Common.convertUnixDate( weatherForecastResult.getList().get(i).dt)));
        myViewHolder.desc.setText( new StringBuffer( weatherForecastResult.getList().get(i).weather.get(0).getDescription()));
        myViewHolder.temp.setText( new StringBuffer( String.valueOf( weatherForecastResult.getList().get(i).main.getTemp())).append("°C"));




    }

    @Override
    public int getItemCount() {
            return weatherForecastResult.getList().size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView day,temp,desc;
        ImageView imageViewForecast;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            imageViewForecast = itemView.findViewById(R.id.img_weather_forecast);
            day = itemView.findViewById( R.id.txt_weakDay);
            temp = itemView.findViewById( R.id.txt_temperature_forecast);
            desc = itemView.findViewById( R.id.txt_description_weather_forecast);
        }
    }
}
