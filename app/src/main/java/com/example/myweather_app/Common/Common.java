package com.example.myweather_app.Common;

import android.location.Location;

import com.example.myweather_app.Model.WeatherResult;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Common {

    public static final String API_ID="ee83ba42f76874bbd063ef27009111c3";
    public static Location current_location= null;

    public static String convertUnixDate(long dt) {
        Date date = new Date(dt*1000);
        SimpleDateFormat sdf = new SimpleDateFormat( "HH:mm  EEE dd MM yyyy");
        String formated = sdf.format( date );
        return formated;
    }

    public static String convertUnixHour(long dt) {
        Date date = new Date(dt*1000);
        SimpleDateFormat sdf = new SimpleDateFormat( "HH:mm");
        String formated = sdf.format( date );
        return formated;
    }
}
