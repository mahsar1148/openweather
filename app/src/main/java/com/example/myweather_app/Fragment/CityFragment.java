package com.example.myweather_app.Fragment;


import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.util.SimpleArrayMap;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.InputQueue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myweather_app.Common.Common;
import com.example.myweather_app.Model.WeatherResult;
import com.example.myweather_app.R;
import com.example.myweather_app.Retrofit.IopenWeatherMap;
import com.example.myweather_app.Retrofit.RetroFitClient;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.label305.asynctask.SimpleAsyncTask;
import com.mancj.materialsearchbar.MaterialSearchBar;
import com.squareup.picasso.Picasso;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.GZIPInputStream;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 */
public class CityFragment extends Fragment {

    private List<String> listCity;
    private MaterialSearchBar materialSearchBar;
    ImageView imageView;
    TextView txt_wind,txt_humid,txt_date,txt_sunrise,txt_sunset,txt_cityName,txt_per,txt_temp,txt_desc,txt_geo;
    ProgressBar progressBar;
    IopenWeatherMap myServise;
    CompositeDisposable compositeDisposable;
    LinearLayout linearLayout;


    static CityFragment instance;
    public static CityFragment getInstance() {
        if( instance == null)
            instance = new CityFragment();
        return instance;
    }

    public CityFragment() {
        compositeDisposable = new CompositeDisposable();
        Retrofit retrofit = RetroFitClient.getInstance();
        myServise = retrofit.create(IopenWeatherMap.class);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_city, container, false);



        imageView = view.findViewById(R.id.img_weather_city_fragment);
        txt_wind = view.findViewById(R.id.txt_wind_city_fragment);
        txt_humid = view.findViewById(R.id.txt_Humidity_city_fragment);
        txt_date = view.findViewById(R.id.txt_date_time_city_fragment);
        txt_sunrise = view.findViewById(R.id.txt_Sunrise_city_fragment);
        txt_sunset = view.findViewById(R.id.txt_Sunset_city_fragment);
        txt_cityName = view.findViewById(R.id.txt_city_name_city_fragment);
        txt_per = view.findViewById(R.id.txt_pressure_city_fragment);
        txt_temp = view.findViewById(R.id.txt_temperature_city_fragment);
        txt_desc = view.findViewById(R.id.txt_description_city_fragment);
        txt_geo = view.findViewById(R.id.txt_geo_coord_city_fragment);
        progressBar = view.findViewById(R.id.loading_city);
        linearLayout = view.findViewById(R.id.weather_panel_city_name);
        materialSearchBar = view.findViewById(R.id.search_bar_city);
        materialSearchBar.setEnabled(false);

        new LoadCities().execute(); //AsynTask For Load City List


        return view;
    }

    private class LoadCities extends SimpleAsyncTask<List<String >> {
        @Override
        protected List<String> doInBackgroundSimple() {

            listCity = new ArrayList<>();
            try {
                StringBuffer builder = new StringBuffer();
                InputStream is = getResources().openRawResource( R.raw.city_list);
                GZIPInputStream gzipInputStream = new GZIPInputStream( is );

                InputStreamReader reader = new InputStreamReader( gzipInputStream );
                BufferedReader in = new BufferedReader( reader );

                String read;

                while ( ( read = in.readLine()) !=null )
                    builder.append( read );

                    listCity = new Gson().fromJson( builder.toString() , new TypeToken< List <String>>() {}.getType());


            } catch (IOException e) {
                e.printStackTrace();
            }

            return listCity;
        }

        @Override
        protected void onSuccess(final List<String> listCities) {
            super.onSuccess(listCities);
            materialSearchBar.setEnabled( true );
            materialSearchBar.addTextChangeListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                    List<String > suggest = new ArrayList<>();

                    for ( String  search : listCities)
                    {
                        if( search.toLowerCase().contains( materialSearchBar.getText().toLowerCase()));
                        suggest.add( search );
                    }
                    materialSearchBar.setLastSuggestions( suggest );
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            materialSearchBar.setOnSearchActionListener(new MaterialSearchBar.OnSearchActionListener() {
                @Override
                public void onSearchStateChanged(boolean enabled) {

                }

                @Override
                public void onSearchConfirmed(CharSequence text) {

                    getWeatherInformation(text.toString());
                    materialSearchBar.setLastSuggestions( listCities );
                }

                @Override
                public void onButtonClicked(int buttonCode) {

                }
            });

            materialSearchBar.setLastSuggestions( listCities );
            progressBar.setVisibility(View.GONE);

        }
    }

    private void getWeatherInformation(String cityName) {


        compositeDisposable.add( myServise.getWeatherByCityName(
                cityName
                ,Common.API_ID
                ,"metric")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<WeatherResult>() {
                               @Override
                               public void accept(WeatherResult weatherResult) throws Exception {

                                   //Load Image
                                   Picasso.get().load( new StringBuffer("https://openweathermap.org/img/w/")
                                           .append(weatherResult.getWeather().get(0).getIcon())
                                           .append(".png").toString()).into( imageView );

                                   //Load Data
                                   txt_cityName.setText( weatherResult.getName());
                                   txt_desc.setText( new StringBuffer("weather in")
                                           .append( weatherResult.getName()).toString());
                                   txt_temp.setText( new StringBuffer( String.valueOf( weatherResult.getMain().getTemp())).append("°C").toString());
                                   txt_date.setText( Common.convertUnixDate(weatherResult.getDt()));
                                   txt_per.setText( new StringBuffer( String.valueOf( weatherResult.getMain().getPressure())).append(" hpa ").toString());
                                   txt_humid.setText( new StringBuffer( String.valueOf( weatherResult.getMain().getHumidity())).append(" %").toString());
                                   txt_sunrise.setText( Common.convertUnixHour( weatherResult.getSys().getSunrise()));
                                   txt_sunset.setText( Common.convertUnixHour( weatherResult.getSys().getSunset()));
                                   txt_geo.setText( new StringBuffer("[").append( weatherResult.getCoord().toString()).append("]").toString());

                                   linearLayout.setVisibility( View.VISIBLE);
                                   progressBar.setVisibility(View.GONE);


                               }
                           }, new Consumer<Throwable>() {
                               @Override
                               public void accept(Throwable throwable) throws Exception {

                                   Toast.makeText(getActivity(), ""+throwable.getMessage(), Toast.LENGTH_SHORT).show();
                               }
                           }
                )
        );

    }


//    @Override
//    public void onDestroy() {
//        compositeDisposable.clear();
//        super.onDestroy();
//    }
//
//
//    @Override
//    public void onStop() {
//        compositeDisposable.clear();
//        super.onStop();
//    }
}
