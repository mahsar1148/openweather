package com.example.myweather_app.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.example.myweather_app.Adapter.Weather_Forecast_Adapter;
import com.example.myweather_app.Common.Common;
import com.example.myweather_app.Model.WeatherForecastResult;
import com.example.myweather_app.R;
import com.example.myweather_app.Retrofit.IopenWeatherMap;
import com.example.myweather_app.Retrofit.RetroFitClient;
import com.example.myweather_app.volley.VolleyForecastWeather;
import com.example.myweather_app.volley.Whther5day;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 */
public class ForecastFragment extends Fragment {

    CompositeDisposable compositeDisposable;
    IopenWeatherMap myService;
    TextView textViewCity , textViewGeo;
    RecyclerView recyclerView;


    static ForecastFragment instance;
    public static ForecastFragment getInstance() {
        if( instance == null)
            instance = new ForecastFragment();
        return instance;
    }


    public ForecastFragment() {
        compositeDisposable = new CompositeDisposable();
        Retrofit retrofit = RetroFitClient.getInstance();
        myService = retrofit.create(IopenWeatherMap.class);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       View view= inflater.inflate(R.layout.fragment_forecast, container, false);
       textViewCity = view.findViewById(R.id.txt_city_name_forecast);
       textViewGeo = view.findViewById(R.id.txt_geo_coord_forecast);
       recyclerView = view.findViewById(R.id.recycler_forecast);
       recyclerView.setLayoutManager( new LinearLayoutManager( getContext() , LinearLayoutManager.VERTICAL ,false));
       getWeatherForecastInformation();
       return view;
    }

    private void getWeatherForecastInformation() {

        VolleyForecastWeather.currentForecastWhther(new Whther5day() {
            @Override
            public void onSuccess(WeatherForecastResult weatherForecastResult) {
                displayForecastWeather( weatherForecastResult);
            }

            @Override
            public void onFailed(VolleyError error) {
                Toast.makeText(getActivity(), ""+error.toString(), Toast.LENGTH_SHORT).show();

            }
        },getActivity(),Common.current_location.getLatitude(), Common.current_location.getLongitude(),"ee83ba42f76874bbd063ef27009111c3");



    //***************************************************************************
//        compositeDisposable.add( myService.getForecastWeatherByLatLng(
//                String.valueOf(Common.current_location.getLatitude()),
//                String.valueOf( Common.current_location.getLongitude()),
//                Common.API_ID,
//                "metric")
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new Consumer<WeatherForecastResult>() {
//                    @Override
//                    public void accept(WeatherForecastResult weatherForecastResult) throws Exception {
//                        displayForecastWeather( weatherForecastResult);
//
//
//                    }
//                }, new Consumer<Throwable>() {
//                    @Override
//                    public void accept(Throwable throwable) throws Exception {
//                        Log.e("Error" ,""+throwable.getMessage());
//                        throwable.printStackTrace();
//                    }
//                })
//        );
    }

    private void displayForecastWeather(WeatherForecastResult weatherForecastResult) {
         textViewCity.setText( new StringBuffer( weatherForecastResult.getCity().getName()));
         textViewGeo.setText( new StringBuffer( weatherForecastResult.getCity().getCoord().toString()));

        Weather_Forecast_Adapter adapter = new Weather_Forecast_Adapter( getContext(),weatherForecastResult );
        recyclerView.setAdapter( adapter );

    }

    @Override
    public void onStop() {
        compositeDisposable.clear();
        super.onStop();
    }
}
