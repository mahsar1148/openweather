package com.example.myweather_app.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.example.myweather_app.Common.Common;
import com.example.myweather_app.Model.WeatherResult;
import com.example.myweather_app.R;
import com.example.myweather_app.Retrofit.IopenWeatherMap;
import com.example.myweather_app.Retrofit.RetroFitClient;
import com.example.myweather_app.volley.VollayCurentWhther;
import com.example.myweather_app.volley.Whtherlitener;
import com.squareup.picasso.Picasso;

import org.reactivestreams.Subscriber;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 */
public class TodayWeatherFragment extends Fragment {

    ImageView imageView;
    TextView txt_wind,txt_humid,txt_date,txt_sunrise,txt_sunset,txt_cityName,txt_per,txt_temp,txt_desc,txt_geo;
    ProgressBar progressBar;
    IopenWeatherMap myServise;
    CompositeDisposable compositeDisposable;
    LinearLayout linearLayout;

    static TodayWeatherFragment instance;
    public static TodayWeatherFragment getInstance() {
        if( instance == null)
            instance = new TodayWeatherFragment();
        return instance;
    }

    public TodayWeatherFragment() {
        compositeDisposable = new CompositeDisposable();
        Retrofit retrofit = RetroFitClient.getInstance();
        myServise = retrofit.create(IopenWeatherMap.class);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_today_weather, container, false);
  //_____________________________________________________________________________________________

        imageView = view.findViewById(R.id.img_weather);
        txt_wind = view.findViewById(R.id.txt_wind);
        txt_humid = view.findViewById(R.id.txt_Humidity);
        txt_date = view.findViewById(R.id.txt_date_time);
        txt_sunrise = view.findViewById(R.id.txt_Sunrise);
        txt_sunset = view.findViewById(R.id.txt_Sunset);
        txt_cityName = view.findViewById(R.id.txt_city_name);
        txt_per = view.findViewById(R.id.txt_pressure);
        txt_temp = view.findViewById(R.id.txt_temperature);
        txt_desc = view.findViewById(R.id.txt_description);
        txt_geo = view.findViewById(R.id.txt_geo_coord);
        progressBar = view.findViewById(R.id.loading);
        linearLayout = view.findViewById(R.id.weather_panel);
        getWeatherInformation();
   //_______________________________________________________________________________________________
        return view;
    }

    private void getWeatherInformation() {

        VollayCurentWhther.currentWhther(new Whtherlitener() {
            @Override
            public void onSuccess(WeatherResult weatherResult) {
                Picasso.get().load( new StringBuffer("https://openweathermap.org/img/w/")
                        .append(weatherResult.getWeather().get(0).getIcon())
                        .append(".png").toString()).into( imageView );

                //Load Data
                txt_cityName.setText( weatherResult.getName());
                txt_desc.setText( new StringBuffer("weather in")
                        .append( weatherResult.getName()).toString());
                txt_temp.setText( new StringBuffer( String.valueOf( weatherResult.getMain().getTemp())).append("°C").toString());
                txt_date.setText( Common.convertUnixDate(weatherResult.getDt()));
                txt_per.setText( new StringBuffer( String.valueOf( weatherResult.getMain().getPressure())).append(" hpa ").toString());
                txt_humid.setText( new StringBuffer( String.valueOf( weatherResult.getMain().getHumidity())).append(" %").toString());
                txt_sunrise.setText( Common.convertUnixHour( weatherResult.getSys().getSunrise()));
                txt_sunset.setText( Common.convertUnixHour( weatherResult.getSys().getSunset()));
                txt_geo.setText( new StringBuffer( weatherResult.getCoord().toString()).toString());


                linearLayout.setVisibility( View.VISIBLE);
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailed(VolleyError error) {
                Toast.makeText(getActivity(), ""+error.toString(), Toast.LENGTH_SHORT).show();

            }
        },getActivity(),Common.current_location.getLatitude(),Common.current_location.getLongitude(),"ee83ba42f76874bbd063ef27009111c3");
//*************************************************************************************************
    }

}
