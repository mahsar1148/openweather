package com.example.myweather_app.Model;

import java.util.List;

public class MyList {

   public int dt ;
   public Main1 main ;
   public List<Weather> weather;
   private Clouds clouds ;
   private Wind wind ;
//   public Rain rain ;
   public Sys1 sys ;

   private String dt_txt ;

    public int getDt() {
        return dt;
    }

    public void setDt(int dt) {
        this.dt = dt;
    }

    public Main1 getMain() {
        return main;
    }

    public void setMain(Main1 main) {
        this.main = main;
    }

    public List<Weather> getWeather() {
        return weather;
    }

    public void setWeather(List<Weather> weather) {
        this.weather = weather;
    }

    public Clouds getClouds() {
        return clouds;
    }

    public void setClouds(Clouds clouds) {
        this.clouds = clouds;
    }

    public Wind getWind() {
        return wind;
    }

    public void setWind(Wind wind) {
        this.wind = wind;
    }

    public Sys1 getSys() {
        return sys;
    }

    public void setSys(Sys1 sys) {
        this.sys = sys;
    }

    public String getDt_txt() {
        return dt_txt;
    }

    public void setDt_txt(String dt_txt) {
        this.dt_txt = dt_txt;
    }
}
