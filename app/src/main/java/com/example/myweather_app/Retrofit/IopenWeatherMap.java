package com.example.myweather_app.Retrofit;

import com.example.myweather_app.Model.WeatherForecastResult;
import com.example.myweather_app.Model.WeatherResult;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface IopenWeatherMap {

    @GET("weather")
    Observable<WeatherResult> getWeatherByLatLng(@Query("lat") String lat ,
                                                 @Query("lon") String lng ,
                                                 @Query("appid") String appid ,
                                                 @Query("units") String unit);

    @GET("weather")
    Observable<WeatherResult> getWeatherByCityName(@Query("q") String cityName ,
                                                 @Query("appid") String appid ,
                                                 @Query("units") String unit);


    @GET("forecast")
    Observable<WeatherForecastResult> getForecastWeatherByLatLng(@Query("lat") String lat ,
                                                                 @Query("lon") String lng ,
                                                                 @Query("appid") String appid ,
                                                                 @Query("units") String unit);
}
