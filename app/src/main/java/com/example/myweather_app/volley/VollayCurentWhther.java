package com.example.myweather_app.volley;

import android.content.Context;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.example.myweather_app.Model.WeatherForecastResult;
import com.example.myweather_app.Model.WeatherResult;

public class VollayCurentWhther {
    public static void currentWhther(final Whtherlitener whtherlitener, Context context, double lat,
                                        double lon, String apiKey) {
        String url = "http://api.openweathermap.org/data/2.5/weather?lat="+lat+"&lon="+lon+"&appid="+apiKey+"&units=metric";
        GsonRequest <WeatherResult>request = new GsonRequest<>(Request.Method.GET, url, WeatherResult.class,
                null,  new Response.Listener<WeatherResult>() {
            @Override
            public void onResponse(WeatherResult response) {
                whtherlitener.onSuccess(response);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                whtherlitener.onFailed(error);
            }
        }, Request.Priority.NORMAL);

        request.setRetryPolicy(new DefaultRetryPolicy(10000, 1, 1.0F));
        VolleyController.INSTANCE.addToRequestQueue(context, request);

    }
}
