package com.example.myweather_app.volley;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.util.LruCache;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;



/**
 * Created by shuaib1413 on 13/04/2016.
 */
public enum VolleyController {
    INSTANCE;

    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;

    VolleyController() {
        VolleyLog.DEBUG = false;
    }

    synchronized RequestQueue getRequestQueue(Context context) {
        if (mRequestQueue == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            mRequestQueue = Volley.newRequestQueue(context.getApplicationContext());
        }
        return mRequestQueue;
    }


    <T> void addToRequestQueue(Context context, Request<T> req) {
//        req.setShouldRetryServerErrors(true);
//        req.setTag(magnetAd);
        req.setRetryPolicy(new DefaultRetryPolicy(2500, 2, 1));
        getRequestQueue(context).add(req);
    }

}

