package com.example.myweather_app.volley;

import android.content.Context;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.example.myweather_app.Model.WeatherForecastResult;
import com.example.myweather_app.Model.WeatherResult;

public class VolleyForecastWeather {


    public static void currentForecastWhther(final Whther5day whther5day, Context context, double lat,
                                     double lon, String apiKey) {
        String url = "http://api.openweathermap.org/data/2.5/forecast?lat="+lat+"&lon="+lon+"&appid="+apiKey+"&units=metric";
        GsonRequest <WeatherForecastResult>request = new GsonRequest<>(Request.Method.GET, url, WeatherForecastResult.class,
                null,  new Response.Listener<WeatherForecastResult>() {
            @Override
            public void onResponse(WeatherForecastResult response) {
                whther5day.onSuccess(response);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                whther5day.onFailed(error);
            }
        }, Request.Priority.NORMAL);

        request.setRetryPolicy(new DefaultRetryPolicy(10000, 1, 1.0F));
        VolleyController.INSTANCE.addToRequestQueue(context, request);

    }

}
