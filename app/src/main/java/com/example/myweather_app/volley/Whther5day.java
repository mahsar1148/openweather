package com.example.myweather_app.volley;

import com.android.volley.VolleyError;
import com.example.myweather_app.Model.WeatherForecastResult;

public interface Whther5day {

    void onSuccess(WeatherForecastResult weatherForecastResult);
    void onFailed(VolleyError error);
}
